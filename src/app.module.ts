import { Global, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { CombinationService, CheckService } from './src/checkCombs';

@Global()
@Module({
  controllers: [AppController],
  providers: [CombinationService, CheckService],
})
export class AppModule {}
