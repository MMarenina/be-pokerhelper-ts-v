export interface Transform {
  value?: number;
  suit?: string;
  count: number;
}
