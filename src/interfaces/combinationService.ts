import { Card } from './card';

export interface ICombinationService {
  checkSeniorCombination(cards: Card[]): string;
}
