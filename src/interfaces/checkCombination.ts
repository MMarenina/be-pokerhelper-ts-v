import { Card } from './';
export interface IntCheckCombination {
  isCorrectData(cards: Card[]): boolean;
  isHighCard(cards: Card[]): boolean;
  isPairsCombs(cards: Card[]): boolean;
  isTwoPairsCombs(cards: Card[]): boolean;
  isThreeCombs(cards: Card[]): boolean;
  isStraight(cards: Card[]): boolean;
  isFlush(cards: Card[]): boolean;
  isFourKindCombs(cards: Card[]): boolean;
  isStraightFlush(cards: Card[]): boolean;
  isRoyalFlush(cards: Card[]): boolean;
}
