import { Card, Transform } from '../../interfaces';

export class TransformersCard {
  static transformCards(cards: Card[], prop: string): Transform[] {
    const cardsSort = cards.sort(( a, b) => b.value - a.value);
    const propertyValueArr = cardsSort.map(e => e[prop]);
    const propertySet = [...new Set(propertyValueArr)];
    return propertySet.map(e => {
      return {
        [prop]: e,
        count: propertyValueArr.filter(a => a === e).length,
      };
    }).sort( ( a, b) => b.count - a.count);
  }

  static transformFlush(cards: Card[]): Card[] {
    const newCards = this.transformCards(cards, 'suit');
    return newCards[0].count >= 5 ? cards.filter(card => card.suit === newCards[0].suit) : [];
  }
}
