import { Injectable } from '@nestjs/common';
import { Card, IntCheckCombination } from '../../../interfaces';
import { TransformersCard } from '../../transformers/transformersCard';

@Injectable()
export class CheckService implements IntCheckCombination {
  public isCorrectData(cards: Card[]): boolean {
    if (cards.length < 5 || cards.length > 7) {
      return true;
    }
  }

  public isHighCard(cards: Card[]): boolean {
    if (cards.length !== 0) {
      return true;
    }
  }

  public isPairsCombs(cards: Card[]): boolean {
    const preparedCards = TransformersCard.transformCards(cards, 'value');
    if (preparedCards[0].count >= 2) {
      return true;
    }
  }

  public isTwoPairsCombs(cards: Card[]): boolean {
    const preparedCards = TransformersCard.transformCards(cards, 'value');
    if (preparedCards[0].count >= 2 && preparedCards[1].count >= 2) {
      return true;
    }
  }

  public isThreeCombs(cards: Card[]): boolean {
    const preparedCards = TransformersCard.transformCards(cards, 'value');
    if (preparedCards[0].count >= 3) {
      return true;
    }
  }

  public isStraight(cards: Card[]): boolean {
    const cardsSort = cards.sort((a, b) => b.value - a.value);
    const propertyValueArr = cardsSort.map(e => e.value);
    const propertySet = [...new Set(propertyValueArr)];
    for (let i = 0; i < propertySet.length - 4; i++) {
      if (propertySet[i] === propertySet[i + 4] + 4) {
        return true;
      }
    }
  }

  public isFlush(cards: Card[]): boolean {
    const preparedCardsSuit = TransformersCard.transformCards(cards, 'suit');
    if (preparedCardsSuit[0].count >= 5) {
      return true;
    }
  }

  public isFullHouse(cards: Card[]): boolean {
    const preparedCards = TransformersCard.transformCards(cards, 'value');
    if (preparedCards[0].count >= 3 && preparedCards[1].count >= 2 && preparedCards.length !== 1) {
      return true;
    }
  }

  public isFourKindCombs(cards: Card[]): boolean {
    const preparedCards = TransformersCard.transformCards(cards, 'value');
    if (preparedCards[0].count >= 4) {
      return true;
    }
  }

  public isStraightFlush(cards: Card[]): boolean {
    const flushCards = TransformersCard.transformFlush(cards);
    for (let i = 0; i < flushCards.length - 4; i++) {
      if (flushCards[i].value === flushCards[i + 4].value + 4) {
        return true;
      }
    }
  }

  public isRoyalFlush(cards: Card[]): boolean {
    const flushCards = TransformersCard.transformFlush(cards);
    if (flushCards.length >= 5) {
      if (flushCards[0].value === flushCards[4].value + 4 && flushCards[0].value === 14) {
        return true;
      }
    }
  }
}
