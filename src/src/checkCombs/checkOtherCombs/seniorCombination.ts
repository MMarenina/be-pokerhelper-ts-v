import { Card, ICombinationService } from '../../../interfaces/';
import { Injectable } from '@nestjs/common';
import { CheckService } from '..';

@Injectable()
export class CombinationService implements ICombinationService {
  constructor(private checkCombs: CheckService) {}
  public checkSeniorCombination(cards: Card[]): string {
    const combinationObj = {
      'Select 5-7 cards': this.checkCombs.isCorrectData,
      'Royal Flush': this.checkCombs.isRoyalFlush,
      'Straight-flush': this.checkCombs.isStraightFlush,
      'Four of a Kind': this.checkCombs.isFourKindCombs,
      'Full House': this.checkCombs.isFullHouse,
      'Flush': this.checkCombs.isFlush,
      'Straight': this.checkCombs.isStraight,
      'Three of a Kind': this.checkCombs.isThreeCombs,
      'Two Pair': this.checkCombs.isTwoPairsCombs,
      'One Pair': this.checkCombs.isPairsCombs,
      'High Card': this.checkCombs.isHighCard,
    };
    const keys = Object.keys(combinationObj);
    return keys.find(key => {
      return combinationObj[key](cards);
    });
  }
}
